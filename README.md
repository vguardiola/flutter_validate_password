# flutter_validate_password

A Flutter demo application to animate password validation.

### Result
[APK](./media/app.apk)

[Video](./media/validation.mp4)

![](./media/Screenshot01.png)

![](./media/Screenshot02.png)

### Key Code

- Use Transform.rotate for incline the piece of paper.
- For animations we use a AnimationController, a CurvedAnimation and a Tween object.
  - AnimationController is used to indicate the duration and it has the method to start the animation.
  ```dart
    _animationLengthController = new AnimationController(duration: Duration(milliseconds: 500), vsync: this);
  ```
  - CurvedAnimation is the animation type.
  ```dart
    CurvedAnimation curve = new CurvedAnimation(parent: _animationLengthController, curve: Curves.easeOut);
  ```
  - Tween generate values between begin and end attributes and the values are generate based in the animation type passed to animate method.
  ```dart
    _animationLength = new Tween(begin: 0.0, end: 1.0).animate(curve)..addListener(() => setState(() {}));
  ```


