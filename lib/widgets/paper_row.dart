import 'package:flutter/material.dart';

class PaperRow extends StatelessWidget {
  final double lineWidth;
  final String rowText;

  const PaperRow({Key key, this.lineWidth = 0.0, this.rowText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(
              width: 30.0,
              height: 40.0,
              child: Container(
                child: Opacity(
                  opacity: lineWidth,
                  child: Icon(
                    Icons.check,
                    color: Colors.green,
                  ),
                ),
                decoration: BoxDecoration(
                  border: Border(
                    right: BorderSide(color: Colors.red),
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 5.0),
              child: Stack(
                children: <Widget>[
                  Text(
                    rowText,
                    style: TextStyle(
                      color: Colors.black,
                      letterSpacing: 1,
                      decoration: TextDecoration.none,
                    ),
                  ),
                  Positioned(
                    child: Container(
                      width: (200 * lineWidth),
                      child: Text(
                        rowText,
                        maxLines: 1,
                        style: TextStyle(
                          color: Colors.grey,
                          letterSpacing: 1,
                          decoration: TextDecoration.lineThrough,
                          decorationColor: Colors.green,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Divider(
          height: 1,
          color: Colors.blue,
        ),
      ],
    );
  }
}
