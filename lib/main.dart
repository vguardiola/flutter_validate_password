import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_validate_password/widgets/paper_row.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Validation Password'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  bool _showFinalTick;
  AnimationController _animationLengthController;
  Animation<double> _animationLength;
  AnimationController _animationUpperController;
  Animation<double> _animationUpper;
  AnimationController _animationNumberController;
  Animation<double> _animationNumber;
  AnimationController _animationSpecialController;
  Animation<double> _animationSpecial;

  @override
  void initState() {
    super.initState();
    _showFinalTick = false;
    _animationLengthController = new AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    CurvedAnimation curve = new CurvedAnimation(parent: _animationLengthController, curve: Curves.easeOut);
    _animationLength = new Tween(begin: 0.0, end: 1.0).animate(curve)..addListener(() => setState(() {}));
    _animationUpperController = new AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    curve = new CurvedAnimation(parent: _animationUpperController, curve: Curves.easeOut);
    _animationUpper = new Tween(begin: 0.0, end: 1.0).animate(curve)..addListener(() => setState(() {}));
    _animationNumberController = new AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    curve = new CurvedAnimation(parent: _animationNumberController, curve: Curves.easeOut);
    _animationNumber = new Tween(begin: 0.0, end: 1.0).animate(curve)..addListener(() => setState(() {}));
    _animationSpecialController = new AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    curve = new CurvedAnimation(parent: _animationSpecialController, curve: Curves.easeOut);
    _animationSpecial = new Tween(begin: 0.0, end: 1.0).animate(curve)..addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.deepPurple,
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(60),
            child: Transform.rotate(
              angle: math.pi / -18,
              child: Stack(
                children: <Widget>[
                  SizedBox(
                    height: 184,
                    child: Container(
                      child: Card(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            PaperRow(
                              rowText: '     8 caracteres     ',
                              lineWidth: _animationLength.value,
                            ),
                            PaperRow(
                              rowText: '     1 mayúscula     ',
                              lineWidth: _animationUpper.value,
                            ),
                            PaperRow(
                              rowText: '     1 número     ',
                              lineWidth: _animationNumber.value,
                            ),
                            PaperRow(
                              rowText: '     1 carácter especial     ',
                              lineWidth: _animationSpecial.value,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  _showFinalTick == true
                      ? Positioned(
                          right: 16,
                          bottom: 16,
                          child: Material(
                            type: MaterialType.circle,
                            color: Colors.green,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.check,
                                color: Colors.white,
                                size: 32,
                              ),
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: TextField(
              key: Key('pass'),
              obscureText: true,
              onChanged: _checkPasswordStrong,
              autofocus: true,
              cursorColor: Colors.blue,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)), borderSide: BorderSide(color: Colors.white)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)), borderSide: BorderSide(color: Colors.white)),
                filled: true,
                fillColor: Colors.white,
                hintText: 'Set a password',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 40.0),
            width: MediaQuery.of(context).size.width,
            child: FlatButton(
              onPressed: () {},
              color: Colors.orangeAccent,
              child: Text(
                'SAVE',
                style: TextStyle(
                  color: Colors.deepPurple,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _checkPasswordStrong(String value) {
    final RegExp regexNum = new RegExp(r"\d");
    final RegExp regexUpperCase = new RegExp(r"[A-Z]");
    final RegExp regexSpecial = new RegExp(r"[!@$%&^*\(\)]");
    final bool isCorrectLength = value.length >= 8;
    final bool hasNumber = regexNum.hasMatch(value);
    final bool hasUppercase = regexUpperCase.hasMatch(value);
    final bool hasSpecialChar = regexSpecial.hasMatch(value);
    _animValidation(isCorrectLength, _lastStatus(_animationLength.value), _animationLengthController);
    _animValidation(hasNumber, _lastStatus(_animationNumber.value), _animationNumberController);
    _animValidation(hasUppercase, _lastStatus(_animationUpper.value), _animationUpperController);
    _animValidation(hasSpecialChar, _lastStatus(_animationSpecial.value), _animationSpecialController);
    setState(() {
      _showFinalTick = hasSpecialChar && hasNumber && hasUppercase && isCorrectLength;
    });
  }

  bool _lastStatus(value) => !(value == 0);

  void _animValidation(isOk, lastStatus, animationController) {
    if (lastStatus != isOk) {
      if (isOk) {
        animationController.forward();
      } else {
        animationController.reverse();
      }
    }
  }
}
